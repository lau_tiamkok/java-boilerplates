# Java

1. Install Java jdk.

2. Open CMD,

    `cd C:\[path]\`

    Compile,

    `javac MyFirstJavaProgram.java`

    Run,

    `java -cp . MyFirstJavaProgram`

    or

    ```
    cd ..
    java -cp ./[folder-name];. MyFirstJavaProgram
    ```

    Result,

    `Hello World`

## ref:

    * http://stackoverflow.com/questions/1906445/what-is-the-difference-between-jdk-and-jre
    * http://www.javatpoint.com/difference-between-jdk-jre-and-jvm
    * http://stackoverflow.com/questions/7485670/error-could-not-find-or-load-main-class
    * http://javarevisited.blogspot.com/2015/04/error-could-not-find-or-load-main-class-helloworld-java.html
