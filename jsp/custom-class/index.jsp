<html>
<head>
<title>Custom Class</title>
</head>
<body>

    <%@ page import="java.io.*,java.util.*" %>
    <%@ page import="javax.servlet.*,java.text.*" %>
    <%@ page import="Foo.Hello" %>

    <%--
    When you have made new changes to the same class, you have to clean your project and
    redeploy it on your server and also clean and restart your server.

    http://stackoverflow.com/questions/31934268/java-and-jsp-java-lang-nosuchmethoderror
    --%>

    <%
    Date date = new Date();
    out.print( "<p>" +date.toString()+"</p>");

    SimpleDateFormat ft = new SimpleDateFormat ("EEEE");
    out.print( "<p>" + ft.format(date) + "</p>");
    %>

    <%
    Hello hello = new Hello();

    hello.setMessage("Hello There!");
    out.print(hello.getMessage() + "<br/>");
    out.print(hello.sayHello());
    %>

</body>
</html>
