<html>
<head><title>Using GET Method to Read Form Data</title></head>
<body>

    <%--
      http://localhost:8080/index.jsp?first_name=ZARA&last_name=ALI
    --%>

    <h1>Using GET Method to Read Form Data</h1>
    <ul>
      <li>
        <p><b>First Name:</b>
         <%= request.getParameter("first_name")%>
        </p>
      </li>
      <li>
        <p><b>Last  Name:</b>
         <%= request.getParameter("last_name")%>
        </p>
      </li>
    </ul>
</body>
</html>
