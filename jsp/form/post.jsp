<html>
<head><title>Using GET Method to Read Form Data</title></head>
<body>

    <%--
      http://localhost:8080/index.jsp?first_name=ZARA&last_name=ALI
    --%>

    <form action="get.jsp" method="POST">
      First Name: <input type="text" name="first_name">
      <br />
      Last Name: <input type="text" name="last_name" />
      <input type="submit" value="Submit" />
    </form>
</body>
</html>
