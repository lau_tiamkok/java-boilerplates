# JSP

1. Setting up Java Development Kit.

2. Setting up Web Server: Tomcat.

    JSP-files are Java Server Pages files. You need servlet container to run them (e.g. tomcat).

    Download latest version of Tomcat from http://tomcat.apache.org/

3. Once you downloaded the installation, unpack the binary distribution into a convenient location. For example in C:\Apache\tomcat-8.0.24 on windows

4. Create CATALINA_HOME environment variable pointing to these locations.

    name - CATALINA_HOME
    value - `C:\Apache\tomcat-8.0.24`

5. Start the server.

    Tomcat can be started by executing the following commands on windows machine:

    `%CATALINA_HOME%\bin\startup.bat`

     or

     `C:\Apache\tomcat-8.0.24\bin\startup.bat`

     Now access it from the browser at,

     `http://localhost:8080/`

6. Run the jsp files.

    Let us keep above code in JSP file hello.jsp and put this file in **C:\Apache\tomcat-8.0.24\webapps\ROOT** directory and try to browse it by giving URL http://localhost:8080/hello.jsp.

## ref:

    * http://www.tutorialspoint.com/jsp/index.htm
    * http://www.jsptut.com/
    * http://stackoverflow.com/questions/2032622/wamp-equivalent-for-jsp
    * http://superuser.com/questions/288398/how-to-run-the-jsp-file-in-wampserver
