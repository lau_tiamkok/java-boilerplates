# App Development

1. WEB-INF

    WEB-INF directory is inside your webapp (one WEB-INF per webapp). You can either deploy the complete directory structure or you can construct a single WAR file.

2. Configure your server.xml file and put 2 services so that you can run different apps on single Tomcat instance behind different ports.

    for instance,

    * app1 on http://localhost:8081
    * app2 on http://localhost:8082

    ```
    <Service name="app1">
       <Connector port="8081" protocol="org.apache.coyote.http11.Http11NioProtocol"
               connectionTimeout="20000"
               redirectPort="8443" />
       <Engine name="Catalina" defaultHost="localhost">
          <Host name="localhost"  appBase="app1"
            unpackWARs="true" autoDeploy="true">
          </Host>
       </Engine>
    </Service>
    <Service name="app2">
       <Connector port="8082" protocol="org.apache.coyote.http11.Http11NioProtocol"
               connectionTimeout="20000"
               redirectPort="8443" />
       <Engine name="Catalina" defaultHost="localhost">
          <Host name="localhost"  appBase="app2"
            unpackWARs="true" autoDeploy="true">
          </Host>
       </Engine>
    </Service>
    ```

3. Restart the Tomcat server and create the app directory in `C:\Apache\tomcat-8.0.24`

    for instance,

    ```
    app1/
        ROOT/
            WEB-INF/
                class/
                    Foo/
                      Hello.class
                      Hello.java
            index.jsp
    ```

    Note: one WEB-INF per app.

## ref:

    * http://stackoverflow.com/questions/8823290/how-to-run-different-apps-on-single-tomcat-instance-behind-different-ports
