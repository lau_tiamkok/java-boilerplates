package Foo;

public class Hello
{
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public String sayHello ()
    {
        return "Hello World!";
    }
}
