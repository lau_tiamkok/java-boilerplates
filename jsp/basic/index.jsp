<html>
<head><title>Hello World</title></head>
<body>
    <%-- This comment will not be visible in the page source --%>
    Hello World!<br/>
    <% out.println("Your IP address is " + request.getRemoteAddr()); %>

    <%--
    A JSP expression element contains a scripting language expression that is evaluated,
    converted to a String, and inserted where the expression appears in the JSP file.

    Following is the syntax of JSP Expression:

    <%= expression %>

    --%>

    <p>Today's date: <%= (new java.util.Date()).toLocaleString() %></p>

    <p>Today's date: <%= (new java.util.Date()).toString() %></p>

    <p>Today's day of week: <%= (new java.util.GregorianCalendar()).get(new java.util.GregorianCalendar().DAY_OF_WEEK) %></p>

    <%--
    Handle dates in java/jsp
    http://www.tutorialspoint.com/jsp/jsp_handling_date.htm
    http://stackoverflow.com/questions/5270272/how-to-determine-day-of-week-by-passing-specific-date
    http://stackoverflow.com/questions/3790954/how-to-get-localized-short-day-in-week-name-mo-tu-we-th
    --%>

    <%@ page import="java.io.*,java.util.*" %>
    <%@ page import="javax.servlet.*,java.text.*" %>
    <%
      Date date = new Date();
      out.print( "<p>" +date.toString()+"</p>");

      SimpleDateFormat ft = new SimpleDateFormat ("EEEE");
      out.print( "<p>" + ft.format(date) + "</p>");
    %>

    <%--
    http://www.easywayserver.com/blog/java-calendar-find-day-of-week/

    /*
      Sunday Day of Week 1
      Monday Day of Week 2
      Tuesday Day of Week 3
      Wednesday Day of Week 4
      Thrusday Day of Week 5
      Friday Day of Week 6
      Saturday Day of Week 7
    */
    --%>

    <%
      // Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
      Calendar localCalendar = new GregorianCalendar();
      int currentDayOfWeek = localCalendar.get(Calendar.DAY_OF_WEEK);
    %>

    <%
    int day = currentDayOfWeek;
    out.println(day);
    %>

    <% if (day == 1 | day == 7) { %>
          <p> Today is weekend</p>
    <% } else { %>
          <p> Today is not weekend</p>
    <% } %>

    <%
    switch(day) {
    case 1:
       out.println("It\'s Sunday.");
       break;
    case 2:
       out.println("It\'s Monday.");
       break;
    case 3:
       out.println("It\'s Tuesday.");
       break;
    case 4:
       out.println("It\'s Wednesday.");
       break;
    case 5:
       out.println("It\'s Thursday.");
       break;
    case 6:
       out.println("It\'s Friday.");
       break;
    default:
       out.println("It's Saturday.");
    }
    %>
</body>
</html>
